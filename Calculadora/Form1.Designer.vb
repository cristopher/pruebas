﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TxtCalc = New System.Windows.Forms.TextBox()
        Me.Btn1 = New System.Windows.Forms.Button()
        Me.Btn7 = New System.Windows.Forms.Button()
        Me.Btn9 = New System.Windows.Forms.Button()
        Me.Btn4 = New System.Windows.Forms.Button()
        Me.Btn8 = New System.Windows.Forms.Button()
        Me.Btn6 = New System.Windows.Forms.Button()
        Me.Btn2 = New System.Windows.Forms.Button()
        Me.Btn3 = New System.Windows.Forms.Button()
        Me.Btn5 = New System.Windows.Forms.Button()
        Me.Btn0 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.Button17 = New System.Windows.Forms.Button()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.Button20 = New System.Windows.Forms.Button()
        Me.Button21 = New System.Windows.Forms.Button()
        Me.Button22 = New System.Windows.Forms.Button()
        Me.Button23 = New System.Windows.Forms.Button()
        Me.Button24 = New System.Windows.Forms.Button()
        Me.Button25 = New System.Windows.Forms.Button()
        Me.Button26 = New System.Windows.Forms.Button()
        Me.Button28 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'TxtCalc
        '
        Me.TxtCalc.BackColor = System.Drawing.SystemColors.MenuBar
        Me.TxtCalc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtCalc.Font = New System.Drawing.Font("Arial", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtCalc.Location = New System.Drawing.Point(12, 12)
        Me.TxtCalc.Name = "TxtCalc"
        Me.TxtCalc.Size = New System.Drawing.Size(189, 39)
        Me.TxtCalc.TabIndex = 0
        Me.TxtCalc.Text = "0"
        Me.TxtCalc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Btn1
        '
        Me.Btn1.Location = New System.Drawing.Point(12, 201)
        Me.Btn1.Name = "Btn1"
        Me.Btn1.Size = New System.Drawing.Size(33, 30)
        Me.Btn1.TabIndex = 1
        Me.Btn1.Text = "1"
        Me.Btn1.UseVisualStyleBackColor = True
        '
        'Btn7
        '
        Me.Btn7.Location = New System.Drawing.Point(12, 129)
        Me.Btn7.Name = "Btn7"
        Me.Btn7.Size = New System.Drawing.Size(33, 30)
        Me.Btn7.TabIndex = 2
        Me.Btn7.Text = "7"
        Me.Btn7.UseVisualStyleBackColor = True
        '
        'Btn9
        '
        Me.Btn9.Location = New System.Drawing.Point(90, 129)
        Me.Btn9.Name = "Btn9"
        Me.Btn9.Size = New System.Drawing.Size(33, 30)
        Me.Btn9.TabIndex = 3
        Me.Btn9.Text = "9"
        Me.Btn9.UseVisualStyleBackColor = True
        '
        'Btn4
        '
        Me.Btn4.Location = New System.Drawing.Point(12, 165)
        Me.Btn4.Name = "Btn4"
        Me.Btn4.Size = New System.Drawing.Size(33, 30)
        Me.Btn4.TabIndex = 4
        Me.Btn4.Text = "4"
        Me.Btn4.UseVisualStyleBackColor = True
        '
        'Btn8
        '
        Me.Btn8.Location = New System.Drawing.Point(51, 129)
        Me.Btn8.Name = "Btn8"
        Me.Btn8.Size = New System.Drawing.Size(33, 30)
        Me.Btn8.TabIndex = 5
        Me.Btn8.Text = "8"
        Me.Btn8.UseVisualStyleBackColor = True
        '
        'Btn6
        '
        Me.Btn6.Location = New System.Drawing.Point(90, 165)
        Me.Btn6.Name = "Btn6"
        Me.Btn6.Size = New System.Drawing.Size(33, 30)
        Me.Btn6.TabIndex = 6
        Me.Btn6.Text = "6"
        Me.Btn6.UseVisualStyleBackColor = True
        '
        'Btn2
        '
        Me.Btn2.Location = New System.Drawing.Point(51, 201)
        Me.Btn2.Name = "Btn2"
        Me.Btn2.Size = New System.Drawing.Size(33, 30)
        Me.Btn2.TabIndex = 7
        Me.Btn2.Text = "2"
        Me.Btn2.UseVisualStyleBackColor = True
        '
        'Btn3
        '
        Me.Btn3.Location = New System.Drawing.Point(90, 201)
        Me.Btn3.Name = "Btn3"
        Me.Btn3.Size = New System.Drawing.Size(33, 30)
        Me.Btn3.TabIndex = 8
        Me.Btn3.Text = "3"
        Me.Btn3.UseVisualStyleBackColor = True
        '
        'Btn5
        '
        Me.Btn5.Location = New System.Drawing.Point(51, 165)
        Me.Btn5.Name = "Btn5"
        Me.Btn5.Size = New System.Drawing.Size(33, 30)
        Me.Btn5.TabIndex = 9
        Me.Btn5.Text = "5"
        Me.Btn5.UseVisualStyleBackColor = True
        '
        'Btn0
        '
        Me.Btn0.Location = New System.Drawing.Point(12, 237)
        Me.Btn0.Name = "Btn0"
        Me.Btn0.Size = New System.Drawing.Size(72, 30)
        Me.Btn0.TabIndex = 10
        Me.Btn0.Text = "0"
        Me.Btn0.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(90, 237)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(33, 30)
        Me.Button10.TabIndex = 11
        Me.Button10.Text = ","
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(12, 93)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(33, 30)
        Me.Button11.TabIndex = 12
        Me.Button11.Text = "←"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(12, 57)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(33, 30)
        Me.Button12.TabIndex = 13
        Me.Button12.Text = "MC"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(51, 93)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(33, 30)
        Me.Button13.TabIndex = 14
        Me.Button13.Text = "CE"
        Me.Button13.UseVisualStyleBackColor = True
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(51, 57)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(33, 30)
        Me.Button14.TabIndex = 15
        Me.Button14.Text = "MR"
        Me.Button14.UseVisualStyleBackColor = True
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(90, 93)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(33, 30)
        Me.Button15.TabIndex = 16
        Me.Button15.Text = "C"
        Me.Button15.UseVisualStyleBackColor = True
        '
        'Button16
        '
        Me.Button16.Location = New System.Drawing.Point(90, 57)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(33, 30)
        Me.Button16.TabIndex = 17
        Me.Button16.Text = "MS"
        Me.Button16.UseVisualStyleBackColor = True
        '
        'Button17
        '
        Me.Button17.Location = New System.Drawing.Point(129, 57)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(33, 30)
        Me.Button17.TabIndex = 18
        Me.Button17.Text = "M+"
        Me.Button17.UseVisualStyleBackColor = True
        '
        'Button18
        '
        Me.Button18.Location = New System.Drawing.Point(129, 93)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(33, 30)
        Me.Button18.TabIndex = 19
        Me.Button18.Text = "±"
        Me.Button18.UseVisualStyleBackColor = True
        '
        'Button19
        '
        Me.Button19.Location = New System.Drawing.Point(129, 129)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(33, 30)
        Me.Button19.TabIndex = 20
        Me.Button19.Text = "/"
        Me.Button19.UseVisualStyleBackColor = True
        '
        'Button20
        '
        Me.Button20.Location = New System.Drawing.Point(129, 165)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(33, 30)
        Me.Button20.TabIndex = 21
        Me.Button20.Text = "*"
        Me.Button20.UseVisualStyleBackColor = True
        '
        'Button21
        '
        Me.Button21.Location = New System.Drawing.Point(129, 201)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(33, 30)
        Me.Button21.TabIndex = 22
        Me.Button21.Text = "-"
        Me.Button21.UseVisualStyleBackColor = True
        '
        'Button22
        '
        Me.Button22.Location = New System.Drawing.Point(129, 237)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(33, 30)
        Me.Button22.TabIndex = 23
        Me.Button22.Text = "+"
        Me.Button22.UseVisualStyleBackColor = True
        '
        'Button23
        '
        Me.Button23.Location = New System.Drawing.Point(168, 57)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(33, 30)
        Me.Button23.TabIndex = 24
        Me.Button23.Text = "M-"
        Me.Button23.UseVisualStyleBackColor = True
        '
        'Button24
        '
        Me.Button24.Location = New System.Drawing.Point(168, 93)
        Me.Button24.Name = "Button24"
        Me.Button24.Size = New System.Drawing.Size(33, 30)
        Me.Button24.TabIndex = 25
        Me.Button24.Text = "√"
        Me.Button24.UseVisualStyleBackColor = True
        '
        'Button25
        '
        Me.Button25.Location = New System.Drawing.Point(168, 129)
        Me.Button25.Name = "Button25"
        Me.Button25.Size = New System.Drawing.Size(33, 30)
        Me.Button25.TabIndex = 26
        Me.Button25.Text = "%"
        Me.Button25.UseVisualStyleBackColor = True
        '
        'Button26
        '
        Me.Button26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button26.Location = New System.Drawing.Point(168, 165)
        Me.Button26.Name = "Button26"
        Me.Button26.Size = New System.Drawing.Size(33, 30)
        Me.Button26.TabIndex = 27
        Me.Button26.Text = "1/×"
        Me.Button26.UseVisualStyleBackColor = True
        '
        'Button28
        '
        Me.Button28.Location = New System.Drawing.Point(168, 201)
        Me.Button28.Name = "Button28"
        Me.Button28.Size = New System.Drawing.Size(33, 66)
        Me.Button28.TabIndex = 29
        Me.Button28.Text = "="
        Me.Button28.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(214, 279)
        Me.Controls.Add(Me.Button28)
        Me.Controls.Add(Me.Button26)
        Me.Controls.Add(Me.Button25)
        Me.Controls.Add(Me.Button24)
        Me.Controls.Add(Me.Button23)
        Me.Controls.Add(Me.Button22)
        Me.Controls.Add(Me.Button21)
        Me.Controls.Add(Me.Button20)
        Me.Controls.Add(Me.Button19)
        Me.Controls.Add(Me.Button18)
        Me.Controls.Add(Me.Button17)
        Me.Controls.Add(Me.Button16)
        Me.Controls.Add(Me.Button15)
        Me.Controls.Add(Me.Button14)
        Me.Controls.Add(Me.Button13)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Button11)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Btn0)
        Me.Controls.Add(Me.Btn5)
        Me.Controls.Add(Me.Btn3)
        Me.Controls.Add(Me.Btn2)
        Me.Controls.Add(Me.Btn6)
        Me.Controls.Add(Me.Btn8)
        Me.Controls.Add(Me.Btn4)
        Me.Controls.Add(Me.Btn9)
        Me.Controls.Add(Me.Btn7)
        Me.Controls.Add(Me.Btn1)
        Me.Controls.Add(Me.TxtCalc)
        Me.Name = "Form1"
        Me.Text = "Calculadora"
        Me.TransparencyKey = System.Drawing.Color.White
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TxtCalc As System.Windows.Forms.TextBox
    Friend WithEvents Btn1 As System.Windows.Forms.Button
    Friend WithEvents Btn7 As System.Windows.Forms.Button
    Friend WithEvents Btn9 As System.Windows.Forms.Button
    Friend WithEvents Btn4 As System.Windows.Forms.Button
    Friend WithEvents Btn8 As System.Windows.Forms.Button
    Friend WithEvents Btn6 As System.Windows.Forms.Button
    Friend WithEvents Btn2 As System.Windows.Forms.Button
    Friend WithEvents Btn3 As System.Windows.Forms.Button
    Friend WithEvents Btn5 As System.Windows.Forms.Button
    Friend WithEvents Btn0 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents Button17 As System.Windows.Forms.Button
    Friend WithEvents Button18 As System.Windows.Forms.Button
    Friend WithEvents Button19 As System.Windows.Forms.Button
    Friend WithEvents Button20 As System.Windows.Forms.Button
    Friend WithEvents Button21 As System.Windows.Forms.Button
    Friend WithEvents Button22 As System.Windows.Forms.Button
    Friend WithEvents Button23 As System.Windows.Forms.Button
    Friend WithEvents Button24 As System.Windows.Forms.Button
    Friend WithEvents Button25 As System.Windows.Forms.Button
    Friend WithEvents Button26 As System.Windows.Forms.Button
    Friend WithEvents Button28 As System.Windows.Forms.Button

End Class
